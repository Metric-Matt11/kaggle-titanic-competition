import pandas as pd
import numpy as np
import statsmodels.formula.api as smf
from sklearn.metrics import mean_absolute_error
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import OrdinalEncoder
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
import matplotlib.pyplot as plt

survival_data = pd.read_csv('Datasets/gender_submission.csv')
training_data = pd.read_csv('Datasets/train.csv')
validation_data = pd.read_csv('Datasets/test.csv')

validation_data = pd.merge(survival_data, validation_data, 'inner', on='PassengerId')
training_data = training_data.append(validation_data)
# Thoughts
# Dont include Fare, there are some 1st class with 0 Fare, other Fares dont seem accurate

# testing Area
training_data = training_data.dropna(subset=['Age'])
test = training_data[(training_data['Survived'] == 1)]
test2 = training_data[(training_data['Survived'] == 0)]
plt.hist(test['Age'], 50, alpha=0.5, label='Alive')
plt.hist(test2['Age'], 50, alpha=0.5, label='Dead')
plt.legend(loc='upper right')
plt.show()
# Cleaning Data
training_data = training_data.dropna(subset=['Embarked', 'Age'])
training_data['Cabin'] = training_data['Cabin'].apply(lambda x: 'Z' if pd.isnull(x) else x)
training_data['Age'] = training_data['Age'].apply(lambda x: 1 if x <= 1 else x)

# Adding Columns
ord_enc = OrdinalEncoder()
training_data['Sex_code'] = training_data['Sex'].map(lambda x: 1 if x == 'male' else 0)
training_data['Cabin_Letter'] = training_data['Cabin'].str[0:1]
training_data['Embarked_code'] = ord_enc.fit_transform(training_data[['Embarked']])
training_data['Cabin_code'] = ord_enc.fit_transform(training_data[['Cabin_Letter']])
training_data['Private_Class'] = training_data['Ticket'].map(lambda pc: 1 if pc[0:2] == 'PC' else 0)
training_data['Infant_flag'] = training_data['Age'].map(lambda kid: 1 if kid <= 8 else 0)

# training_data['Has_family'] =

# Understanding Relationships

model_data = training_data.drop(
    columns=['Survived', 'PassengerId', 'Name', 'Sex', 'Ticket', 'Cabin', 'Embarked', 'Cabin_Letter', 'Age'])

Correlation_Matrix = training_data.corr()

# Testing Models
# linear model
model = smf.ols('Survived ~ Sex_code', data=training_data)
results = model.fit()
print(results.params)

# Trying to loop through different models
x = model_data
y = training_data.Survived
train_x, val_x, train_y, val_y = train_test_split(x, y, random_state=0)
models = []
models.append(('RF', RandomForestClassifier(random_state=0)))
models.append(('LR', LogisticRegression(solver='liblinear', multi_class='ovr')))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC(gamma='auto')))
# evaluate each model in turn
results = []
names = []
for name, model in models:
    kfold = StratifiedKFold(n_splits=10, random_state=1, shuffle=True)
    cv_results = cross_val_score(model, train_x, train_y, cv=kfold, scoring='accuracy')
    results.append(cv_results)
    names.append(name)
    # model.fit(train_x, train_y)
    # print(model.predict(val_x))
    print('%s: %f (%f)' % (name, cv_results.mean(), cv_results.std()))
# Compare Algorithms
plt.boxplot(results, labels=names)
plt.title('Algorithm Comparison')
plt.show()
